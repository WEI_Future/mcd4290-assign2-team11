// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];


class Run
{
    constructor()
    {
        this._startLocation=[]
        this._endLocation=[]
        this._pathTakenByUser=[]
        this._currentLocation=[]
        
        
        this._startTime="1/1/2019 Monday 0:0:0"
        this._endTime="1/1/2019 Monday 0:0:0"
        

    }
    
    set startLocation(newStartLocation)
    {
        this._startLocation=newStartLocation
    }
    
    get startLocation()
    {
        return this._startLocation
    }
    
    set endLocation(newEndLocation)
    {
        this._endLocation=newEndLocation
    }
    
    get endLocation()
    {
        return this._endLocation
    }
    
    set pathTakenByUser(newArray)
    {
        for (var i=2;i<newArray.length;i++){
            this._pathTakenByUser.push(newArray[i])
        }
    }
    
    get pathTakenByUser()
    {
        return this._pathTakenByUser
    }
    
    
    
    set startTime(newStartTime)
    {
        this._startTime=newStartTime
        
    }
    
    get startTime()
    {
        return this._startTime
    }
    
    set endTime(newEndTime)
    {
        this._endTime=newEndTime
    }
    
    get endTime()
    {
        return this._endTime
    }
    
    distanceTraveled()
    {
        
        return (startToEndDistance*1000-10)
    }
    
    duration(hs,he,ms,me,ss,se)
    {
        var timeTaken=he*3600+me*60+se-(hs*3600+ms*60+ss)
        var displayTimeTaken=timeTaken+" seconds"
        return displayTimeTaken
    }
}

function beginRun()
{
    //This "begin run" button should be disabled until the user has selected a destination!
    runStarted=true
        
    newRun.startLocation=arrayOfLocations[0]
    newRun.startTime=time()
        
        
    hrStart=dtStart.getHours();
    minStart=dtStart.getMinutes();
    secStart=dtStart.getSeconds();      
}

function save()
{
    if (runEnded===true && typeof(Storage)!=="undefined"){
        localStorage.setItem(APP_PREFIX,JSON.stringify(newRun))
        var newRunObject=JSON.parse(localStorage.getItem(APP_PREFIX))
        savedRuns.push(newRunObject)
        
        return newRunObject
    }
    
}



function showPosition(position)
{
    lat=position.coords.latitude
    long=position.coords.longitude  
    acc=position.coords.accuracy
       
    
    let updatedLocation=[long,lat];
    //add start location,destination location and path locations to an Array!
    arrayOfLocations.push(updatedLocation) 
    
    
    if(arrayOfLocations.length===1){
        arrayOfStartAndEndLocations.push(updatedLocation)
        map.panTo(arrayOfLocations[0])
        
        var marker=new mapboxgl.Marker()
        marker.setLngLat(arrayOfLocations[0])
        marker.addTo(map)
        
        var popUp=new mapboxgl.Popup()
        popUp.setText("Start")
        marker.setPopup(popUp)     //set the labels to the marker
        popUp.addTo(map)      //always appears
        
        document.getElementById("locationAccuracy").innerHTML = acc + "m"
        document.getElementById("nowTime").innerHTML=time()
    }
        
    if(runStarted===true){
        newRun.pathTakenByUser.push(updatedLocation)
        
        var marker=new mapboxgl.Marker()
        marker.setLngLat(updatedLocation)
        marker.addTo(map)
        
        var popUp=new mapboxgl.Popup()
        
        var toStartDistance=1000*getDistanceFromLatLonInKm(lat,long,arrayOfStartAndEndLocations[0][1],arrayOfStartAndEndLocations[0][0])
        var toEndDistance=1000*getDistanceFromLatLonInKm(lat,long,arrayOfStartAndEndLocations[1][1],arrayOfStartAndEndLocations[1][0])
        popUp.setText("path,distance to start:"+toStartDistance+"m,distance to end:"+toEndDistance+"m,time:"+time())
        document.getElementById("distanceLeft").innerHTML=toEndDistance+"m"
        document.getElementById("distanceCovered").innerHTML=toStartDistance+"m"
        document.getElementById("nowTime").innerHTML=time()
        
        
        //Completing a run
        if (toEndDistance<=10){
            runEnded=true
            newRun.endTime=time()
            
            hrEnd=dtEnd.getHours();
            minEnd=dtEnd.getMinutes();
            secEnd=dtEnd.getSeconds();
            
            popUp.setText("Time Taken:"+newRun.duration(hrStart,hrEnd,minStart,minEnd,secStart,secEnd)+",Distance Travelled:"+newRun.distanceTraveled())                    //Display time taken and distance travelled
        }
        marker.setPopup(popUp)     //set the labels to the marker
        popUp.addTo(map)      //always appears
        
        document.getElementById("nowTime").innerHTML=time()
        document.getElementById("saveButton").disabled = false;
    }
     
                
    
}

// This function checks whether there is a map layer with id matching 
// idToRemove.  If there is, it is removed.
function removeLayerWithId(idToRemove)
{
    let hasPoly = map.getLayer(idToRemove)
    if (hasPoly !== undefined)
    {
        map.removeLayer(idToRemove)
        map.removeSource(idToRemove)
    }
}
  

function time(){
	
   var dt=new Date();
   var year=dt.getYear()+1900;
   var month=dt.getMonth()+1;
   var d=dt.getDate();
   var weekday=["Sunday","Monday","Thesday","Wednesday","Thursday","Friday","Saturday"];
   var day=dt.getDay();
   var hr=dt.getHours();
   var min=dt.getMinutes();
   var sec=dt.getSeconds();
   if(hr<10){
     hr="0"+hr;
    }
   if(min<10){
     min="0"+min;
    }
   if(sec<10){
     sec="0"+sec;
    }
   return (d + "/" + month + "/" + year+ "/" + weekday[day] + "" + hr + ":" + min + ":" + sec + "")
}


function randomDestination(LngLatOfCurrentLocation)
{ 
    while (startToEndDistance<0.06){
        //There are 4 possible areas. We should make the possibility 25%.
        var xValue=Math.random()
        var yValue=Math.random()
        
        var randomLongChange,randomLatChange;
        //The destination locates in first area(eastNorth)
        if ((xValue>=0.5) && (yValue>=0.5)){
            var randomLongChange=LngLatOfCurrentLocation[0]+0.001*Math.random()
            var randomLatChange=LngLatOfCurrentLocation[1]+0.001*Math.random()
        }
        //The destination locates in second area(westNorth)
        else if ((xValue<0.5) && (yValue>=0.5)){
            var randomLongChange=LngLatOfCurrentLocation[0]-0.001*Math.random()
            var randomLatChange=LngLatOfCurrentLocation[1]+0.001*Math.random()
        }
        //The destination locates in third area(westSouth)
        else if ((xValue<0.5) && (yValue<0.5)){
            var randomLongChange=LngLatOfCurrentLocation[0]-0.001*Math.random()
            var randomLatChange=LngLatOfCurrentLocation[1]-0.001*Math.random()
        }
        //The destination locates in forth area(eastSouth)
        else if((xValue>=0.5) && (yValue<0.5)){
            var randomLongChange=LngLatOfCurrentLocation[0]+0.001*Math.random()
            var randomLatChange=LngLatOfCurrentLocation[1]-0.001*Math.random()
        }
        
        var Destination=[randomLongChange,randomLatChange];
        startToEndDistance=getDistanceFromLatLonInKm(randomLatChange,randomLongChange,LngLatOfCurrentLocation[1],LngLatOfCurrentLocation[0]);
        
    }
    
    

    arrayOfLocations.push(Destination)
    arrayOfStartAndEndLocations.push(Destination)
    
    if(randomDestinationCreated===false){
        var marker=new mapboxgl.Marker()
        marker.setLngLat(Destination)
        marker.addTo(map)
    
        var popUp=new mapboxgl.Popup()
        popUp.setText("Destination")
        marker.setPopup(popUp)     //set the labels to the marker
        popUp.addTo(map)      //always appears
        document.getElementById("beginRunButton").disabled = false;
        
    }
    
    document.getElementById("distanceLeft").innerHTML=startToEndDistance*1000+"m"
    document.getElementById("distanceCovered").innerHTML=0+"m"
    document.getElementById("totalDistance").innerHTML=startToEndDistance*1000+"m"
    document.getElementById("nowTime").innerHTML=time()
    showPath()
    return Destination
    
}


//show the path between start and destination
function showPath()
{
            // Code added here will run when the "destinationButton" button is clicked.
            
                map.addLayer({             //All are necessary!!!
                    "id": "route",
                    "type": "line",
                    "source": {
                        "type": "geojson",
                        "data": {
                            "type": "Feature",
                            "properties": {},
                            "geometry": {
                                "type": "LineString",
                                "coordinates": [
                                        arrayOfStartAndEndLocations[0],
                                        arrayOfStartAndEndLocations[1]
                                    
                                ]
                            }
                        }
                    },
                    "layout": {
                        "line-join": "round",
                        "line-cap": "round"
                    },
                    "paint": {
                        "line-color": "#888",
                        "line-width": 8
                    }
                });
            
}

//haversine formula,calculate the distance between 2 locations
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
} 