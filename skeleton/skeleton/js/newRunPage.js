// Code for the Measure Run page.

var lat,long,acc;
var arrayOfLocations=[];
var arrayOfStartAndEndLocations=[]
var randomDestinationCreated=false;
var startToEndDistance=0;

var dtStart=new Date();
var hrStart,minStart,secStart;
var dtEnd=new Date();
var hrEnd,minEnd,secEnd;

var newRun=new Run()
var runStarted=false;
var runEnded=false;

mapboxgl.accessToken = 'pk.eyJ1IjoiZnV0dXJlbHciLCJhIjoiY2p5amd0cnpzMDNsYTNuczJsYzFiazlmeiJ9._wNf4wZSMX2ZPM7xhyFGMQ';
let caulfield=[145.1114362,-37.9239318];
    
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 16,
    center:caulfield
});
  
map.addControl(new mapboxgl.GeolocateControl({
  positionOptions: {
    enableHighAccuracy: true
  },
  trackUserLocation: true
}));

navigator.geolocation.watchPosition(showPosition)