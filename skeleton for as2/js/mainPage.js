// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

retrieveRunList();

var node = document.createElement("LI");
node.className = "mdl-list__item mdl-list__item--two-line"
node.onClick = "viewRun(" + n + ")"
var divSet = document.createElement('div');
node.appendChild(divSet);
document.getElementById("runsList").appendChild(node);

// for(i=0;i<savedRuns.length;i++){
//   divSet.className = ""
//   divSet.value = "hi";
// }

               // Create a <li> node
       // Create a text node

   // Append <li> to <ul> with id="myList"

function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}
