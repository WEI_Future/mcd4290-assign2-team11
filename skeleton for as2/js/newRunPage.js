// Code for the Measure Run page.

mapboxgl.accessToken = 'pk.eyJ1IjoicGVyZWdyaW5hdGUiLCJhIjoiY2p6dW8xdTc1MDA2bzNibjI5NTFsd2hrbyJ9.kxSZKE3GTUwecxqJAQrnpA';

//CLEAR: arrayOfLocations, destinationArray, lines 28/29, CLEAR TEST FUNCTION
//[145.1308405, -37.912641],[145.1329004, -37.9118667]
let monashClaytonLngLat = [145.1329004, -37.9118667];
var arrayOfLocations = [[145.1308405, -37.912641],[145.1329004, -37.9118667]], destinationArray = [[145.1362585, -37.9128781]], positionMarkers = [], destinationMarkers = [], acc, buttonFlag=true, runStart=false, firstFlag = false;

//creates new Map object
let map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v10',
  zoom: 16,
  center: monashClaytonLngLat
});

map.addControl(new mapboxgl.GeolocateControl({
  positionOptions: {
    enableHighAccuracy: true
  },
  trackUserLocation: true
}));

positionMarkers.push(new mapboxgl.Popup())
positionMarkers.push(new mapboxgl.Marker())
destinationMarkers.push(new mapboxgl.Popup())
destinationMarkers.push(new mapboxgl.Marker())

//starts watching user's location

var run = new Run();

navigator.geolocation.watchPosition(realTime)

do {
  setInterval(navigator.geolocation.getCurrentPosition(success), 10000);
} while (runStart == true)

document.getElementById("destinationButton").disabled = false;

function realTime(position)
{
  var lng, lat;
  lat=position.coords.latitude
  lng=position.coords.longitude
  acc=position.coords.accuracy

  let currentLocation=[lng,lat];
  //           important!!!!!!!!!!!!!!!!!!!!!!!!!!

  //only occurs during the first instance of watch position, when the page loads, shows user's first position
  if(runStart==false && firstFlag == false){
    arrayOfLocations.push(currentLocation)
    firstFlag == true

    map.panTo(arrayOfLocations[0])

    positionMarkers[1].setLngLat(arrayOfLocations[0])
    positionMarkers[1].setPopup(positionMarkers[0])
    positionMarkers[1].addTo(map)

    positionMarkers[0].setText("You")    //set the labels to the marker
    positionMarkers[0].addTo(map)     //always appears
  }

  //occurs if user moves without pressing the 'start' button
  else if (runStart==false && firstFlag == true){
    arrayOfLocations[0]=currentLocation

    positionMarkers[1].setLngLat(arrayOfLocations[0])
    positionMarkers[1].setPopup(positionMarkers[0])
    positionMarkers[1].addTo(map)

    positionMarkers[0].setText("You")    //set the labels to the marker
    positionMarkers[0].addTo(map)     //always appears
  }


  else if(runStart==true){
    haversine(1);
    haversine(2);

    if (haversine(1) <= 10){
      endrun();
      clearInterval();
      runStart == false;
      arrayOfLocations.push(currentLocation);
    }
  }

  document.getElementById("locationAccuracy").innerHTML = acc + "m"

  if (buttonFlag==true && acc<20){
    document.getElementById("startEndButton").disabled = false;
    document.getElementById("destinationsButton").disabled = false;
    buttonFlag = false;
  }

}

function success(position){
  var lng, lat;
  lat=position.coords.latitude
  lng=position.coords.longitude

  let currentLocation=[lng,lat];

  arrayOfLocations.push(currentLocation)

  let el = document.createElement('div');
  el.className = 'markerType';

  positionMarkers.push(new mapboxgl.Marker(el))
  positionMarkers[positionMarkers.length-1].setLngLat(currentLocation)
  positionMarkers[positionMarkers.length-1].addTo(map);
}

function startRun(){
  // document.getElementById("destinationButton").disabled = true;
  // document.getElementById("startEndButton").disabled = true;

  navigator.geolocation.getCurrentPosition(success)

  run.startLocation = arrayOfLocations.splice(0);
  run.destinationLocation = destinationArray.splice(0);

  var today = new Date();
  run.startDate = today.getDate()+'-'+ (today.getMonth()+1) +'-'+ today.getFullYear()
  run.startTime = today.getHours() +':'+ today.getMinutes()+':'+ today.getSeconds() +':'+ today.getMilliseconds()

  run.startTimeMS = (today.getHours()*3600000)+(today.getMinutes()*60000)+(today.getSeconds()*1000)+ today.getMilliseconds()

  runStart = true;
}

function endRun(){
  run.userPath = arrayOfLocations.splice(1)

  var today = new Date();
  run.endDate = today.getDate()+'-'+ (today.getMonth()+1) +'-'+ today.getFullYear();
  run.endTime = today.getHours() +':'+ today.getMinutes()+':'+ today.getSeconds() +':'+ today.getMilliseconds()

  run.endTimeMS = (today.getHours()*3600000)+(today.getMinutes()*60000)+(today.getSeconds()*1000)+ today.getMilliseconds()

  let distance = haversine(arrayOfLocations[0]);
  run.distanceTraveled = distance;

  let time = run.endTimeMS - run.startTimeMS;
  time = Math.floor(time / 1000);

  run.timeTaken = time;

  run.averageSpeed = (run.distanceTraveled/run.timeTaken);

  document.getElementById("saveButton").disabled = false;

}

function saveRun(){

  retrieveRunList()

  var newRunNo = (savedRuns.length)+1;
  var newRunName = "Run " + newRunNo;

  run.runID = newRunName;

  savedRuns.push(newRunName);

  if (typeof(Storage) !== "undefined") {

    var jsonStringStuff = JSON.stringify(run);

    // Store
    localStorage.setItem(newRunName, jsonStringStuff);

    jsonStringStuff = JSON.stringify(savedRuns);

    localStorage.setItem("Saved Runs Array", jsonStringStuff)

    window.location.href = "index.html";

  }
  else {
    displayMessage("Sorry, Web Storage not supported!");
  }



}

function randomDestination(){

  var copyArray= [], lastElement = [], output = "", newLocation = 0, distance = 0;

  do{

    //shallow copies to avoid changing elements in original array
    copyArray = arrayOfLocations.map(function(arr) {
      return arr.slice();
    });

    lastElement.push(copyArray[copyArray.length-1])

    //generate a random number from -0.001 to 0.001
    newLocation = (((Math.random() * 2) - 1) * 0.001);

    //adjusts lng by the random number
    lastElement[0][0] = lastElement[0][0]+newLocation

    //repeating for lat
    newLocation = (((Math.random() * 2) - 1) * 0.001);

    //adjusts lat by the random number
    lastElement[0][1] = lastElement[0][1]+newLocation

    distance = haversine(lastElement[0]);

  } while(distance<60);

  output = distance;

  document.getElementById("distanceLeft").innerHTML=output

  destinationArray = lastElement;

  destinationMarkers[1].setLngLat(destinationArray[0])
  destinationMarkers[1].setPopup(destinationMarkers[0])
  destinationMarkers[1].addTo(map)

  destinationMarkers[0].setText("Destination")    //set the labels to the marker
  destinationMarkers[0].addTo(map)

  document.getElementById("startEndButton").disabled = false;

}

function deg2rad(degrees){
  radians = degrees * (Math.PI/180);
  return radians;
}

function haversine(value) {
  var startCoords = [], endCoords = [], secondCoords, label;

  startCoords.push(arrayOfLocations[arrayOfLocations.length - 1]);

  //Current Location to Destination, Distance Left
  if (value == 1) {
    secondCoords = destinationArray[0];
    label = "distanceLeft";
  }

  //Current Location to Start, Distance Covered
  else if(value == 2) {
    secondCoords = arrayOfLocations[0];
    label = "distanceCovered"
  }

  //calculates distance between current location and given location
  else {
    secondCoords = value;
  }

  endCoords.push(secondCoords)

  var lng1 = startCoords[0][0]
  var lat1 = startCoords[0][1]

  var lng2 = endCoords[0][0]
  var lat2 = endCoords[0][1]

  var deltaLat, deltaLng, earthRadius, alpha, beta, a, c, output;

  deltaLat = lat2 - lat1 ;
  deltaLon = lng2 - lng1 ;

  earthRadius =  6369087 ; // in meters 3959 in miles.
  alpha = deltaLat/2;
  beta = deltaLon/2;

  a = Math.sin(deg2rad(alpha)) * Math.sin(deg2rad(alpha)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.sin(deg2rad(beta)) * Math.sin(deg2rad(beta)) ;
  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  distance =  earthRadius * c;

  if(label != undefined){
    output = distance.toFixed(0);
    document.getElementById(label).innerHTML = output;
  }

  else {
    return distance.toFixed(0);
  }
}

function test(){

  retrieveRunList()

  var output = "";

  output += retrieveRun();

//   for (var x in run) {
//   output += run[x];
// }

  document.getElementById("output").innerHTML = output
}
