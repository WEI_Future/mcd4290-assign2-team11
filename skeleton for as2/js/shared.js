// Shared code needed by all three pages.

// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";

// Array of saved Run objects.
var savedRuns = [];

function retrieveRunList(){
  if (localStorage.getItem("Saved Runs Array") === null) {
    return
  }

  else{
  var localeToString = localStorage.getItem("Saved Runs Array");
  savedRuns = JSON.parse(localeToString);
}
}

function retrieveRun()
{

  if (typeof(Storage) !== "undefined")
  {
    var runKey = savedRuns[savedRuns.length-1]
    var localeToString = localStorage.getItem(runKey);
    var reverseObj = JSON.parse(localeToString)

    return reverseObj
  }
  else
  {
    console.log("Error: localStorage is not supported by current browser.");
  }

}



class Run {

  //empty for now, need to receieve variables on load to build from there
  constructor(a){
  if (a===undefined){
      this._runID="" //className

      this._startLocation=[]
      this._destinationLocation=[]
      this._userPath=[]

      this._startDate=""
      this._endDate=""
      this._startTime=""
      this._endTime=""
      this._startTimeMS=0
      this._endTimeMS=0

      this._timeTaken=0
      this._distanceTravelled=0
      this._averageSpeed=0
    }

    else {

      this._runID = a._runID //className

      this._startLocation = a._startLocation
      this._destinationLocation = a._startLocation
      this._userPath = a._userPath

      this._startDate = a._startDate
      this._endDate = a._endDate
      this._startTime = a._startTime
      this._endTime = a._endTime
      this._startTimeMSa = a._startTimeMSa
      this._endTimeMS = a._endTimeMS

      this._timeTaken = a._timeTaken
      this._distanceTravelled = a._distanceTravelled
      this._averageSpeed = a._averageSpeed

    }

  }

  set runID(number){
    this._runID = number
  }

  set startLocation(startLocation){
    this._startLocation=startLocation
  }

  get startLocation(){
    return this._startLocation
  }

  set destinationLocation(destinationLocation){
    this._destinationLocation=destinationLocation
  }

  get destinationLocation(){
    return this._destinationLocation
  }

  set userPath(newArray){
    this._userPath=newArray;
  }

  get userPath(){
    return this._userPath
  }

  set startTime(startTime){
    this._startTime=startTime
  }

  get startTime(){
    return this._startTime
  }

  set startDate(startDate){
    this._startDate=startDate
  }

  get startDate(){
    return this._startDate
  }

  set endTime(endTime){
    this._endTime=endTime
  }

  get endTime(){
    return this._endTime
  }

  set endDate(endDate){
    this._endDate=endDate
  }

  get endDate(){
    return this._endDate
  }

  set startTimeMS(startTimeMS){
    this._startTimeMS=startTimeMS
  }

  get startTimeMS(){
    return this._startTimeMS
  }

  set endTimeMS(endTimeMS){
    this._endTimeMS=endTimeMS
  }

  get endTimeMS(){
    return this._endTimeMS
  }

  set distanceTraveled(distanceTraveled){
    this._distanceTravelled=distanceTraveled
  }

  get distanceTraveled(){
    return this._distanceTravelled
  }

  set timeTaken(timeTaken){
    this._timeTaken=timeTaken
  }

  get timeTaken(){
    return this._timeTaken
  }

  set averageSpeed(averageSpeed){
    this._averageSpeed=averageSpeed
  }

  get averageSpeed(){
    return this._averageSpeed
  }
}
